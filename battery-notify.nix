{config, pkgs, lib, ...}:

{
  systemd.user.services.battery-notify = {
    description = "Notify on low battery";
    path = with pkgs; [ elvish libnotify ];
    serviceConfig = {
      ExecStart = "${./scripts/battery-notify.elv}";
    };
    startAt = "*:0/5:0";
  };
}
