# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./battery-notify.nix
    <home-manager/nixos>
  ];

  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    gc = {
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
  };

  # enable hardware video decoding
  nixpkgs.config.packageOverrides = pkgs: {
    vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  };
  hardware.opengl = {
    extraPackages = with pkgs; [
      intel-media-driver # LIBVA_DRIVER_NAME=iHD
      vaapiIntel # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
      vaapiVdpau
      libvdpau-va-gl
    ];
  };
  # for flashing the ergodox ez with wally
  hardware.keyboard.zsa.enable = true;

  # betray my principles for gamer chat
  nixpkgs.config.allowUnfree = true;

  # Use the systemd-boot EFI boot loader.
  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    kernelParams = [ "intel_pstate=active" ];
    kernelPackages = pkgs.linuxPackages_latest;
  };

  hardware.cpu.intel.updateMicrocode = true;
  hardware.opengl.enable = true;
  hardware.opengl.driSupport = true;
  hardware.opengl.driSupport32Bit = true;

  networking.hostName = "superluminal-steel"; # Define your hostname.
  networking.wireless.enable =
    true; # Enables wireless support via wpa_supplicant.
  networking.wireless.interfaces = [ "wlp3s0" ];
  networking.firewall.enable = false;

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;
  networking.interfaces.wlp3s0.useDHCP = true;
  services.avahi = {
    enable = true;
    nssmdns = true;
  };
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplip ];
  services.gnome.gnome-keyring.enable = true;
  programs.dconf.enable = true;

  # Increase drift time because otherwise it gets annoying.
  services.udev.extraRules = ''
    ACTION=="add", SUBSYSTEM=="input", ATTR{name}=="TPPS/2 IBM TrackPoint", ATTR{device/drift_time}="50"
  '';

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  virtualisation.lxd = {
    enable = true;
    recommendedSysctlSettings = true;
  };

  virtualisation.docker.enable = true;

  fonts.fonts = with pkgs; [
    unifont
    siji
    meslo-lg
    tamzen
    uw-ttyp0
    terminus_font
    ipafont
    kochi-substitute
    (nerdfonts.override { fonts = [ "Iosevka" "Meslo" ]; })
  ];
  fonts.enableDefaultFonts = true;

  console.useXkbConfig = true;

  environment.sessionVariables = {
    TERMINAL = "kitty";
    EDITOR = "nvim";
  };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # pipewire is the Cool New Sound Thing
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };
  # allow screensharing in firefox
  xdg.portal = {
    enable = true;
    extraPortals = with pkgs; [ xdg-desktop-portal-gtk xdg-desktop-portal-wlr ];
    gtkUsePortal = true;
  };

  programs.light.enable = true;
  programs.adb.enable = true;

  services.syncthing = {
    enable = true;
    user = "vector";
    dataDir = "/home/vector/.syncthing";
  };

  boot.initrd.luks.devices = {
    root = {
      device = "/dev/disk/by-uuid/6ea80150-a194-4f2c-a291-c0490efa75a0";
      preLVM = true;
      allowDiscards = true;
    };
  };

  programs.zsh.enable = true;

  services.borgbackup.jobs = let
    makeJob = { repo, passfile, extraArgs ? "" }: rec {
      inherit repo;
      inherit extraArgs;

      paths = "/home/vector";
      encryption = {
        mode = "repokey";
        passCommand = "cat /etc/nixos/${passfile}";
      };
      compression = "auto,zstd";
      startAt = "*-*-* 04:00:00";
      exclude = map (x: "sh:" + paths + "/" + x) [
        ".cache"
        ".cargo"
        "code/*/target"
        "code/others/*/target"
        "**/node_modules"
        "**/.tox"
      ];
      user = "vector";
      group = "users";
      extraCreateArgs = "--stats --checkpoint-interval 600";
      prune.keep = {
        within = "3d";
        daily = 7;
        weekly = 4;
        monthly = 6;
        yearly = 1;
      };
    };
  in {
    key-of-chronology = makeJob {
      repo = "ssh://root@key-of-chronology.local//srv/borg/superluminal-steel";
      passfile = "secrets/key-of-chronology_borg";
    };
    rsync-net = makeJob {
      repo = "ssh://1371@usw-s001.rsync.net/./backups/superluminal-steel";
      passfile = "secrets/rsync-net_borg";
      extraArgs = "--remote-path=borg1";
    };
  };

  users.groups.video = { };

  users.users.vector = {
    isNormalUser = true;
    extraGroups = [ "video" "wheel" "lxd" "docker" "adbusers" "plugdev" ];
    shell = pkgs.elvish;
  };

  home-manager.useGlobalPkgs = true;
  home-manager.users.vector = import ./vector.nix;

  security.pam.services.swaylock = { };

  documentation.man.generateCaches = true;

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?
}
