;; -*- lexical-binding: t -*-

;; an emacs configuration that's meant to be relatively minimal. from
;; someone who likes vim's simplicity and keybindings, but likes
;; emacs's customizability.

;;; Prelude throat-clearing

;; set up security settings. why this isn't the default, I have
;; absolutely no idea.
(with-eval-after-load 'gnutls
  (eval-when-compile
    (require 'gnutls))
  (setq gnutls-verify-error t)
  (setq gnutls-min-prime-bits 3072))
(setq network-security-level 'high)

;; bootstrap package management. this just gets straight.el installed
;; if it isn't already.
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq straight-vc-git-default-clone-depth 1)

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(setq use-package-always-defer t)

(defconst my-leader "SPC")
(defconst my-local-leader "SPC m")

(push "~/.emacs.d/my-lisp" load-path)

;; A nicer setup for defining my own keybindings. Has to go here so I
;; can use `:general'.
(use-package general
  :demand t)

(general-create-definer my-evil-leader-def
  :prefix my-leader
  :states '(normal visual))

(general-create-definer my-evil-local-leader-def
  :prefix my-local-leader
  :states '(normal visual))

;;; Evil keybindings for things that aren't related to a package.
(my-evil-leader-def
  "SPC" 'execute-extended-command

  "b" '(:ignore t :wk "buffer")
  "bb" 'switch-to-buffer
  "bk" 'kill-current-buffer

  "e" '(:ignore t :wk "errors")
  "en" 'next-error
  "ep" 'previous-error

  "f" '(:ignore t :wk "files")
  "fs" 'save-buffer
  "ff" 'find-file)

;;; Appearance

;; We do all the themeing early on so that it applies before we do
;; package-loading and everything else.

(push "~/code/vale.el" custom-theme-load-path)
(load-theme 'vale t)

;; i'm hardcore
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

(set-face-attribute 'default nil
		    :height 113
		    :family "Meslo LG S")

;; I don't start emacs up that much, but this looks pretty.
(use-package dashboard
  :demand t
  :config
  (setq dashboard-items '((projects . 5)
                          (recents . 5))
        dashboard-item-shortcuts '((recents . nil)
                                   (projects . nil))
        dashboard-footer-messages
        '("im gay"
          "remember, always, that you are beloved."
          "if the zoo bans me for hollering at the animals i will face god and walk backwards into hell --@dril"
          "Everything happens so much --@horse_ebooks"
          "Most of you are familiar with the virtues of a programmer. There are three, of course: laziness, impatience, and hubris. --Larry Wall"
          "Escape will make me God. --Durandal"
          "Time is the school in which we learn / Time is the fire in which we burn. --Delmore Schwartz"
          "It takes long winter nights to teach a girl how to cultivate within herself invincible summer days. --Ticker")
        dashboard-banner-logo-title nil
        dashboard-startup-banner 'logo)

  (dashboard-setup-startup-hook))

(use-package blackout
  :straight (:host github :repo "raxod502/blackout")
  ; needs to be demanded so we can do `:blackout t' elsewhere
  :demand t)

(use-package spaceline
  :demand t
  :config
  (setq spaceline-highlight-face-func 'spaceline-highlight-face-evil-state)
  (require 'spaceline-config)
  (spaceline-spacemacs-theme))

;; Supplies a nice distraction-free editor environment.
(use-package writeroom-mode)

;; It adds some nice texture, IMO.
(use-package rainbow-delimiters
  :init
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

;; Lets you do things like hit TAB in M-: to autocomplete variable names.
(setq enable-recursive-minibuffers t)

;;; Moving around

;; Flashes when point moves, the window scrolls, etc.
(use-package beacon
  :demand t
  :config
  (setq beacon-size 80
        beacon-blink-delay 0.15
        beacon-blink-duration 0.15)
  (beacon-mode 1)
  :blackout t)

(use-package avy
  :general
  (my-evil-leader-def
    "jw" 'avy-goto-word-1
    "jl" 'avy-goto-line))
      
;;; Clean up .emacs.d paths

;; `no-littering' attempts to have various packages that store their
;; stuff in `~/.emacs.d' do so in a more principled way.

(use-package no-littering
  :demand t)

;;; org-mode

;; The built-in `org-mode' is ancient; let's use a newer one.

(use-package org)

;;; Selection

;; `selectrum' is like ivy, but intended to be simpler and without as
;; much... *stuff*.
(use-package selectrum
  :init
  (selectrum-mode))

;; `prescient' enables saving history, and `selectrum-prescient'
;; (obviously) integrates the two.
(use-package prescient
  :config
  (prescient-persist-mode)
  (setq prescient-history-length 1000))
(use-package selectrum-prescient
  :demand t
  :after selectrum
  :config
  (selectrum-prescient-mode))

;;; Keybindings

(use-package undo-fu)

(use-package evil-surround
  :after evil
  :demand t
  :config
  (global-evil-surround-mode 1))

;; `which-key' shows what you can do after entering a key sequence. For
;; example, after hitting `C-c @', it shows what various keys do in
;; outline mode.
(use-package which-key
  :demand t
  :config
  (setq which-key-show-early-on-C-h t)
  ; Only show when you hit C-h manually
  (setq which-key-idle-delay 10000)
  (setq which-key-idle-secondary-delay 0.05)
  (which-key-mode 1)
  :blackout t)

;; seriously, why would I want to know that `M-x cu-v` exists
(setq extended-command-suggest-shorter nil)

;; Vim's modal keybindings are nice. Let's use them.
(use-package evil
  :init
  (setq evil-want-keybinding nil)
  (setq evil-undo-system 'undo-fu)
  :demand t
  :config
  (evil-mode))

(use-package evil-colemak-basics
  :after evil
  :demand t
  :config
  (global-evil-colemak-basics-mode))

(use-package evil-collection
  :after (evil magit)
  :demand t
  :config
  (setq evil-collection-company-use-tng nil)
  (evil-collection-init))

;;; Working with projcets
(use-package projectile
  :general
  (my-evil-leader-def
    "p" '(:keymap projectile-command-map :wk "projectile"))
  :config
  ;; gets it to use the default system, which uses selectrum
  (setq projectile-completion-system 'default)
  (projectile-mode)
  :blackout t)

;;; Programming

(use-package yasnippet
  :init
  (yas-global-mode))

;; A great interface for an alright version control system. ;)
(use-package magit
  :general
  (my-evil-leader-def
    "g s" #'magit-status))

;; direnv sets environment variables as you move around directories.
;; We demand it because otherwise it's not clear whether it'd actually
;; get triggered.
(use-package direnv
  :init
  :demand t
  :config
  (setq direnv-always-show-summary nil)
  (direnv-mode)
  :blackout t
  )

;;; Why should IDEs get to have all the fun of autocomplete and everything?
(use-package lsp-mode
  :init
  ; LSP wants these for performance reasons.
  (setq gc-cons-threshold (* 1024 1024 100))
  (setq read-process-output-max (* 1024 1024 4))
  (setq lsp-modeline-code-actions-enable nil)
  (setq lsp-modeline-diagnostics-enable nil)
  (setq lsp-modeline-workspace-status-enable nil)
  (setq lsp-keymap-prefix nil)
  :general
  (my-evil-leader-def
    "l" '(:keymap lsp-command-map :wk "LSP")
    "l =" '(:ignore t :wk "formatting")
    "l T" '(:ignore t :wk "toggles")
    "l s" '(:ignore t :wk "session management")
    "l g" '(:ignore t :wk "go to")
    "l h" '(:ignore t :wk "help")
    "l r" '(:ignore t :wk "refactor")
    "l a" '(:ignore t :wk "action")
    "l F" '(:ignore t :wk "folder")
    "l G" '(:ignore t :wk "peek"))
  :after direnv
  :config
  ; necessary to get LSP and direnv to play nice
  (advice-add 'lsp :before #'direnv-update-environment)
  (setq lsp-signature-doc-lines 5)
  :blackout t
  )

;; `rustic' is an all-in-one mode for working with rust. We initialize
;; it after `lsp-mode' because it wants to interact with LSP.
(use-package rustic
  :after lsp-mode
  :config
  (setq rustic-format-on-save 'on-save)
  ; Prevents displaying rustfmt errors.
  (setq rustic-format-display-method (lambda (buf) ()))
  :general
  (my-evil-local-leader-def
   "c" '(:ignore t :wk "cargo")
   "cb" 'rustic-cargo-build
   "cc" 'rustic-cargo-clippy
   "ck" 'rustic-cargo-check
   "ct" 'rustic-cargo-test)
  :blackout t)

(use-package lua-mode
  :blackout t)

(use-package nix-mode
  :blackout t)

(use-package elvish-mode
  :blackout t)

(use-package flycheck
  :general
  (my-evil-leader-def
    "el" 'flycheck-list-errors)
  :blackout t)

(use-package proof-general
  :init
  (setq coq-compile-before-require t
	coq-one-command-per-line nil
	proof-electric-terminator-enable t))

(use-package company
  :blackout t)

;; Displays popups in a nicer interface.
(use-package company-box
  :init
  (add-hook 'company-mode-hook 'company-box-mode)
  :blackout t)

;;; Version control

;; this hasn't been necessary in *years*
(setq vc-follow-symlinks t)

;; Local Variables:
;; checkdoc-symbol-words: ("top-level")
;; indent-tabs-mode: nil
;; outline-regexp: ";;;+ "
;; sentence-end-double-space: nil
;; End:
