with import <nixpkgs> { };

  stdenv.mkDerivation rec {
    version = "1.0.5";
    name = "mdloader-${version}";
    src = fetchFromGitHub {
      owner = "Massdrop";
      repo = "mdloader";
      rev = version;
      sha256 = "1pxp9l8p1zb1wlsxd81id4pgcsbljiy863dv85dlxcb2wg2iismy";
    };
    buildInputs = [ makeWrapper ];
    installPhase = ''
      mkdir -p $out/bin
      cp build/mdloader $out/bin
      cp applet-mdflash.bin $out/bin
      wrapProgram $out/bin/mdloader --run "cd $out/bin"
    '';
  }

