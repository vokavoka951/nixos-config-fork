{
  network.description = "host for *.catgirl.ai";
  target = { config, pkgs, ... }: {
    imports = [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./nginx.nix
    ];

    boot.loader.grub.enable = true;
    boot.loader.grub.version = 2;

    networking = {
      hostName = "blade-barrage";
      useDHCP = false;
      usePredictableInterfaceNames = false;
      interfaces.eth0.useDHCP = true;
      firewall.allowedTCPPorts = [ 80 443 5000 5222 5269 5280 5281 5347 5582 ];
    };

    users.users.vector = {
      isNormalUser = true;
      home = "/home/vector";
      description = "just this robot girl";
      extraGroups = [ "wheel" "networkmanager" ];
      openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDeCD+KbuTRwY2ImukD13KKARB2XeIYpob5mOZ2cb3xJjD5EsRdixBkbhrNLNyTvGs+OVPM7IiBNHh5qthcEGqUxp10JUAfhQQTEYQL/yMGoAhckbLpIcnn8Ss38iNV7KvXLvOwCVKuC7vsz8hmi50dNoZy8zI7QUXpLgmw2BrbzbtXRceArurYoeItYPaQXmljwLRauh32Jj56owwk5VCFcs64pyryd0KZF8EnPiQAaOboGCl/kwpkfrqMALzdefCeSmjiQQM3v0vMB0BjnZFWW7migO/J5G5zqRUl3qEjFktFLF8o+vp4ctI4FT2IHqeQypANLfVsgX2CGmIGfXJr vector@superluminal-steel"
      ];
    };

    environment.systemPackages = with pkgs; [ inetutils sysstat vim goaccess ];

    programs.mtr.enable = true;

    services.openssh = { enable = true; };

    services.bitwarden_rs = {
      enable = true;
      config = {
        rocketAddress = "127.0.0.1";
        rocketPort = 8175;
      };
    };

    services.miniflux = {
      enable = true;
      config = {
        RUN_MIGRATIONS = "1";
        LISTEN_ADDR = "127.0.0.1:8755";
        BASE_URL = "https://rss.catgirl.ai";
      };
    };

    services.gitea = {
      enable = true;
      database.type = "sqlite3";
      lfs.enable = true;
      appName = "code hosting for synthetic catgirls";
      domain = "git.catgirl.ai";
      rootUrl = "https://git.catgirl.ai";
      httpAddress = "127.0.0.1";
      httpPort = 3000;
      cookieSecure = true;
      disableRegistration = true;
      settings = {
        ui.DEFAULT_THEME = "arc-green";
      };
    };

    services.prosody = {
      enable = true;
      ssl.cert = "/var/lib/acme/prosody/fullchain.pem";
      ssl.key = "/var/lib/acme/prosody/key.pem";
      virtualHosts = {
        "catgirl.ai" = {
          domain = "catgirl.ai";
          enabled = true;
          ssl.cert = "/var/lib/acme/prosody/fullchain.pem";
          ssl.key = "/var/lib/acme/prosody/key.pem";
        };
      };
      muc = [{ domain = "conference.catgirl.ai"; }];
      uploadHttp = { domain = "upload.catgirl.ai"; };
    };
    security.acme.certs.prosody = {
      domain = "catgirl.ai";
      group = "prosody";
      webroot = config.security.acme.certs."catgirl.ai".webroot;
      postRun = "systemctl restart prosody";
      extraDomainNames = [ "upload.catgirl.ai" "conference.catgirl.ai" ];
    };
    # necessary so nginx can do the challenge
    users.users.nginx.extraGroups = [ "prosody" ];

    system.stateVersion = "20.09";
  };
}
