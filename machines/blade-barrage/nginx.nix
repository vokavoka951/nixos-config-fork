{ config, lib, pkgs, ... }:

{
  services.nginx = {
    enable = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;

    # Need the host for splitting out logs via goaccess/etc.
    commonHttpConfig = ''
        log_format vcombined '$host:$server_port '
            '$remote_addr - $remote_user [$time_local] '
            '"$request" $status $body_bytes_sent '
            '"$http_referer" "$http_user_agent"';
        access_log /var/log/nginx/access.log vcombined;
        error_log /var/log/nginx/error.log;
    '';

    virtualHosts."catgirl.ai" = {
      enableACME = true;
      forceSSL = true;
      root = "/var/www/catgirl.ai";
      serverAliases =
        [ "www.catgirl.ai" "synthetic.zone" "www.synthetic.zone" ];
    };

    virtualHosts."bitwarden.catgirl.ai" = {
      useACMEHost = "catgirl.ai";
      forceSSL = true;
      locations."/" = { proxyPass = "http://localhost:8175"; };
    };

    virtualHosts."rss.catgirl.ai" = {
      useACMEHost = "catgirl.ai";
      forceSSL = true;
      locations."/" = { proxyPass = "http://localhost:8755"; };
    };

    virtualHosts."git.catgirl.ai" = {
      useACMEHost = "catgirl.ai";
      forceSSL = true;
      locations."/" = { proxyPass = "http://localhost:3000"; };
    };

    virtualHosts."upload.catgirl.ai" = {
      useACMEHost = "catgirl.ai";
      forceSSL = true;
    };

    virtualHosts."conference.catgirl.ai" = {
      useACMEHost = "catgirl.ai";
      forceSSL = true;
    };
  };

  security.acme.acceptTerms = true;
  security.acme.email = "ext0l@riseup.net";
  security.acme.certs = {
    "catgirl.ai" = {
      extraDomainNames = [
        "rss.catgirl.ai"
        "bitwarden.catgirl.ai"
        "git.catgirl.ai"
        "upload.catgirl.ai"
        "conference.catgirl.ai"
      ];
    };
  };
}
