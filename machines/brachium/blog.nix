{ pkgs, stdenv }:

stdenv.mkDerivation {
  name = "brachium-content";

  src = builtins.fetchGit {
    url = "/home/vector/code/brachium/";
    rev = "9317339101f3026e2a3e72b65a5fa9b49ea130ff";
  };

  buildInputs = [ pkgs.gutenberg ];
  buildPhase = ''
    zola build
  '';
  installPhase = ''
    cp -r public $out
  '';
}
  
