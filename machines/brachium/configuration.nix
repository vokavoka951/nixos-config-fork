# This machine hosts stuff I don't associate with my public identity,
# so the domain is a secret. It's an ordinary nginx webserver so the
# config itself isn't that interesting.
{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ./nginx.nix ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;

  networking.hostName = "brachium";

  networking.useDHCP = false;
  networking.interfaces.eth0.useDHCP = true;
  networking.usePredictableInterfaceNames = false;

  users.users.vector = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" ];
    home = "/home/vector";
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDeCD+KbuTRwY2ImukD13KKARB2XeIYpob5mOZ2cb3xJjD5EsRdixBkbhrNLNyTvGs+OVPM7IiBNHh5qthcEGqUxp10JUAfhQQTEYQL/yMGoAhckbLpIcnn8Ss38iNV7KvXLvOwCVKuC7vsz8hmi50dNoZy8zI7QUXpLgmw2BrbzbtXRceArurYoeItYPaQXmljwLRauh32Jj56owwk5VCFcs64pyryd0KZF8EnPiQAaOboGCl/kwpkfrqMALzdefCeSmjiQQM3v0vMB0BjnZFWW7migO/J5G5zqRUl3qEjFktFLF8o+vp4ctI4FT2IHqeQypANLfVsgX2CGmIGfXJr vector@superluminal-steel"
    ];
  };
  users.users.root.openssh.authorizedKeys.keys =
    config.users.users.vector.openssh.authorizedKeys.keys;

  services.openssh.enable = true;
  services.openssh.permitRootLogin = "yes";

  system.stateVersion = "21.05";
}

