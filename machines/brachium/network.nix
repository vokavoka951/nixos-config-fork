{
  network.description = ":iiam:";

  "brachium" = { config, pkgs, lib, ... }: {
    imports = [ ./configuration.nix ];

    deployment.targetUser = "root";
    deployment.targetHost = builtins.readFile ./domain;
  };
}
