{ config, lib, pkgs, ... }:
let domain = builtins.readFile ./domain;
in {
  networking.firewall.allowedTCPPorts = [ 80 443 ];
  services.nginx = {
    enable = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedOptimisation = true;
    recommendedProxySettings = true;

    # Need the host for splitting out logs via goaccess/etc.
    commonHttpConfig = ''
      log_format vcombined '$host:$server_port '
          '$remote_addr - $remote_user [$time_local] '
          '"$request" $status $body_bytes_sent '
          '"$http_referer" "$http_user_agent"';
      access_log /var/log/nginx/access.log vcombined;
      error_log /var/log/nginx/error.log;
    '';

    virtualHosts.${domain} = {
      enableACME = true;
      forceSSL = true;
      root = pkgs.callPackage ./blog.nix {};
      serverAliases = [ "www.${domain}" ];
    };
  };

  security.acme.acceptTerms = true;
  security.acme.email = "ext0l@riseup.net";
  security.acme.certs = {
    ${domain} = { extraDomainNames = [ "www.${domain}" ]; };
  };
}
