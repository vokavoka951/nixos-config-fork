{ config, pkgs, lib, ... }: {
  nixpkgs.localSystem.system = "aarch64-linux";
  nixpkgs.system = "aarch64-linux";
  imports = [ ../../secrets/wifi.nix ];

  hardware.enableRedistributableFirmware = true;
  hardware.firmware = [ pkgs.wireless-regdb ];
  boot.extraModprobeConfig = ''
    options cfg80211 ieee80211_regdom="US"
  '';
  boot.loader.grub.enable = false;
  boot.loader.generic-extlinux-compatible.enable = true;
  boot.kernelPackages = pkgs.linuxPackages_5_4;
  boot.kernelParams = [ "cma=32M" "console=tty0" ];
  boot.loader.raspberryPi = {
    enable = true;
    version = 3;
    uboot.enable = true;
    firmwareConfig = "gpu_mem=256";
  };
  virtualisation.docker.enable = true;
  networking = {
    firewall.enable = false;
    hostName = "key-of-chronology";
    wireless.enable = true;
    # wifi networks set up in the import
  };

  environment.systemPackages = with pkgs; [ neovim borgbackup ];

  services.avahi = {
    enable = true;
    publish = {
      enable = true;
      addresses = true;
      workstation = true;
    };
  };
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };
  services.openssh = {
    enable = true;
    permitRootLogin = "yes";
  };
  swapDevices = [{
    device = "/swapfile";
    size = 1024;
  }];
  services.syncthing = { enable = true; };
}

