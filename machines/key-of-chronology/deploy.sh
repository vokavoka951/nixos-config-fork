#!/usr/bin/sh
dir=$(dirname $0)
NIXOS_CONFIG=$dir/configuration.nix nixos-rebuild --fast switch --target-host root@key-of-chronology.local --build-host root@key-of-chronology.local
