#!/bin/sh

# Creates a new kitty window named 'scratchpad' if it doesn't exist.
if ! swaymsg -t get_tree | jq -e 'any(.. | .app_id? == "scratchpad")'; then
    kitty --class scratchpad --detach
    # necessary for sway to be able to find it afterwards
    sleep 0.2
fi
swaymsg scratchpad show
