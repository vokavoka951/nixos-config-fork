#!/usr/bin/env elvish

# Displays a notification if the battery is below a certain threshold
# *and* we're not on AC.

var threshold = 10 # percent

cd /sys/class/power_supply

fn is_low {
  if (== 1 (cat AC/online)) {
    put $false
  }
  # average capacity; not quite accurate because it's percentage-wise but eh
  var capacities = [(put BAT* | each [bat]{ cat {$bat}/capacity })]
  > $threshold (/ (+ $@capacities) (count $capacities))
}

try {
  if (is_low) {
    notify-send --urgency critical "Energy supply critical!"
  }
} except e {
  notify-send "battery script failed" $e
}
