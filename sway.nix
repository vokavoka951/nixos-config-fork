{ config, pkgs, lib, ... }:
let
  inherit (lib.lists) imap1;
  inherit (lib.trivial) pipe;
  inherit (builtins) concatLists listToAttrs;
  funcs = import
    <home-manager/modules/services/window-managers/i3-sway/lib/functions.nix> {
      inherit lib;
      cfg = config.wayland.windowManager.sway;
      moduleName = "sway";
    };
  inherit (funcs) keybindingsStr;
in {
  wayland.windowManager.sway = {
    enable = true;
    wrapperFeatures.gtk = true;

    extraConfig = let
      lockedBindingsStr = keybindingsStr {
        keybindings = {
          XF86AudioRaiseVolume =
            "exec pactl set-sink-volume @DEFAULT_SINK@ +10%";
          XF86AudioLowerVolume =
            "exec pactl set-sink-volume @DEFAULT_SINK@ -10%";
          XF86AudioMute = "exec pactl set-sink-mute @DEFAULT_SINK@ toggle";
          XF86AudioPlay = "exec cmus-remote --pause";
          XF86AudioStop = "exec cmus-remote --stop";
          XF86AudioPrev = "exec cmus-remote --prev";
          XF86AudioNext = "exec cmus-remote --next";
          XF86MonBrightnessUp = "exec light -A 5";
          XF86MonBrightnessDown = "exec light -U 5";
        };
        bindsymArgs = "--locked";
      };

    in ''
      for_window [app_id="scratchpad"] move to scratchpad
      exec systemctl --user import-environment
      ${lockedBindingsStr}
    '';

    config = {
      modifier = "Mod4";
      terminal = "${pkgs.kitty}/bin/kitty";
      input = { "type:keyboard" = { xkb_options = "ctrl:nocaps"; }; };
      bars = [{ command = "${pkgs.waybar}/bin/waybar"; }];

      startup = let
        lockCommand =
          "swaylock -f --image ~/Documents/wallpapers/video-games/ivara-sniper.jpg";
      in [{
        command = ''
          swayidle -w \
            timeout 300 '${lockCommand}' \
            before-sleep '${lockCommand}' \
            timeout 360 'swaymsg "output * dpms off"' \
            resume 'swaymsg "output * dpms on"' \
            lock '${lockCommand}' '';
      }

      ];

      gaps.inner = 6;
      window = {
        titlebar = false;
        border = 1;
      };

      keybindings = let
        modifier = "Mod4";
        workspaces = [ "GEN" "SNS" "DEV" "REF" "GBF" "MP3" "MSG" "-8-" "-9-" ];
        workspaceBindings = pipe workspaces [
          (imap1 (i: ws: [
            {
              name = "${modifier}+${toString i}";
              value = "workspace number ${toString i}:${ws}";
            }
            {
              name = "${modifier}+Shift+${toString i}";
              value = "move container to workspace ${toString i}:${ws}";
            }
          ]))
          concatLists
          listToAttrs
        ];
      in lib.mkOptionDefault ({
        "${modifier}+d" = "exec rofi -show drun";
        "${modifier}+minus" = "exec ${./scratchpad.sh}";
        "${modifier}+Backslash" = "sticky toggle";
        "Print" = "exec grimshot copy screen";
        "Shift+Print" = "exec grimshot copy area";
      } // workspaceBindings);

      output."*".bg =
        "~/Documents/wallpapers/anime/alice-marisa-sakuya.jpg fill";
    };
  };
}
