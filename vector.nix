{ config, pkgs, lib, ... }:

{
  imports = [ ./sway.nix ];
  home.packages = with pkgs; [
    # system monitors
    htop
    powertop

    # software dev tools
    ag
    nixfmt
    direnv
    gdb
    jq
    unzip
    nodejs
    perl
    emacs
    tmux
    gnumake
    (import ./external/mdloader.nix)
    bat
    starship
    coq
    morph
    wally-cli # ergodox firmware flasher
    wev
    libnotify # for notify-send
    nmap
    usbutils

    # chat clients
    discord
    tdesktop
    weechat
    gajim
    gnome3.cheese

    thunderbird

    firefox-wayland
    google-chrome # only really use it for GBF
    xdg_utils

    # tunes
    cmus
    mpv
    vlc
    pavucontrol
    pulseaudio # *only* for pactl

    # graphical stuff
    feh
    light
    lxappearance
    toilet
    sway-contrib.grimshot

    bitwarden

    texlive.combined.scheme-full

    magic-wormhole
    borgbackup

    gimp
    blender

    # wayland/sway stuff
    swaylock
    swayidle
    wl-clipboard
    mako # notification daemon

    # I bought aseprite a while ago, it's cool.
    (aseprite.override { unfree = true; })
  ];

  gtk.enable = true;
  gtk.theme = {
    name = "Adwaita-dark";
    package = pkgs.gnome-themes-standard;
  };

  home.file.".elvish" = {
    source = ./elvish;
    recursive = true;
  };

  programs.git = {
    enable = true;
    userEmail = "ext0l@catgirl.ai";
    userName = "Ash";
    aliases = {
      lol = "log --graph --decorate --pretty=oneline --abbrev-commit";
      lola = "lol --all";
    };
    ignores = [ "*~" "\\#*\\#" ];
    extraConfig = { init.defaultBranch = "main"; };
  };

  programs.beets = {
    enable = true;
    settings = {
      directory = "~/music";
      plugins = "fromfilename";
    };
  };

  programs.kitty = {
    enable = true;
    settings = {
      background = "#100d15";
      background_opacity = "0.96";
      font_family = "Meslo LG M";
      font_size = "11.5";
    };
  };

  programs.rofi = {
    enable = true;
    theme = ./interstellar.rasi;
  };

  programs.taskwarrior = {
    enable = true;
    config = {
      # don't show the project notification stuff
      verbose = "blank,label,new-id,affected,edit,special,unwait,recur";
    };
  };

  programs.mako = {
    enable = true;
    defaultTimeout = 6000;
  };

  programs.waybar = {
    enable = true;
    settings = [{
      layer = "top";
      height = 30;
      modules-left = [ "sway/workspaces" "sway/mode" ];
      modules-center = [ "custom/cmus" ];
      modules-right = [
        "custom/backups"
        "custom/beeminder"
        "custom/todoist"
        "cpu"
        "battery"
        "network"
        "clock"
      ];
      modules = {
        "sway/workspaces" = { format = "{name}"; };
        battery = {
          format = "{icon}{capacity:3}%";
          format-icons = [ "" "" "" "" "" ];
          states = { critical = 10; };
        };
        clock = {
          interval = 10;
          format = "{:%a %m/%d %H:%M}";
        };
        cpu = {
          interval = 1;
          format = " {usage:3}%";
        };
        network = {
          interface = "wlp3s0";
          interval = 3;
          format-wifi = "  {essid}";
          format-disconnected = "";
          tooltip-format = "{ipaddr} ({signalStrength}%)";
        };
        "custom/cmus" = {
          interval = 1;
          exec-if = "pgrep -x cmus";
          # `cut` is here because of GY!BE
          exec = ''cmus-remote -C "format_print '%a - %t'" | cut -c-60'';
        };
        "custom/backups" = {
          interval = 60;
          exec = "${./waybar/check-backups.sh} 2>/dev/null";
        };
        "custom/todoist" = {
          interval = 60;
          "format" = "{}";
          exec = "${./waybar/todoist.sh} ${./secrets/todoist-token}";
          on-click = "xdg-open https://todoist.com/app";
        };
        "custom/beeminder" = {
          interval = 60;
          return-type = "json";
          exec = "${./waybar/beeminder.elv} ${./secrets/beeminder-token}";
        };
      };
    }];
    style = builtins.readFile ./waybar/config.css;
  };

  services.lorri = {
    enable = true;
    # revert when https://github.com/NixOS/nixpkgs/pull/119114 goes through
    package = pkgs.callPackage ./external/lorri.nix { };
  };

  services.kanshi = {
    enable = true;
    profiles = let
      resize_scratchpad =
        "${pkgs.sway}/bin/swaymsg '[app_id=scratchpad] resize set 960px 810px, move position center'";

    in {
      undocked.outputs = [{
        criteria = "eDP-1";
        status = "enable";
      }];
      docked.outputs = [
        {
          criteria = "DP-4";
          status = "enable";
        }
        {
          criteria = "eDP-1";
          status = "disable";
        }
      ];
      undocked.exec = resize_scratchpad;
      docked.exec = resize_scratchpad;
    };
  };

}
