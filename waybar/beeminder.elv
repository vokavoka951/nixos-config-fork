#!/usr/bin/env elvish

# Waybar module whose class is 'beemergency' if it's a beemergency,
# and 'ok' otherwise. Takes the path to the API token as the first
# argument.

use str

var token = (cat $args[0])
var url = "https://www.beeminder.com/api/v1/users/hyperstrike/goals.json?auth_token="$token

# How many hours before derailment should this be marked as unsafe?
# Used for habits like going to sleep aren't really useful to display
# as beemergencies earlier. If not specified, assumes Beeminder's
# usual 24-hour derailment.
#
# The key is habit IDs so I don't leak habit names into my public
# config. :p
var thresholds = [
  &606a24293b53bf461c001311=18
  &6098a4ff3b53bf1b4f000264=2
  &603c7b513b53bf6407004550=2
  &603c7c3e3b53bf6489001559=12
]

fn get-threshold [habit]{
  var id = $habit[id]
  if (has-key $thresholds $id) {
    put $thresholds[$id]
  } else {
    put 24
  }
}

fn check-unsafe [habit]{
  var time_left = (- $habit[losedate] (date +%s))
  echo $time_left" seconds until derail for "$habit[slug] >&2
  if (< $time_left (* 3600 (get-threshold $habit))) {
    put $habit[slug]
  }
}

var unsafe = [(
    # API seems flaky
    curl --silent --connect-timeout 3 --retry 2 $url ^
    | from-json | all (one) | each $check-unsafe~
    )]
var class = (if (== (count $unsafe) 0) { put "ok" } else { put "beemergency" })
var tooltip = (if (== (count $unsafe) 0) {
    put "good job :)"
  } else {
    put (str:join "\n" $unsafe)
})
put [
  &class=$class
  &text="\ufbdf " # hexagon-y icon
  &tooltip=$tooltip
] | to-json
