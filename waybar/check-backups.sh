#!/bin/sh

# Checks if backups have succeeded recently. Output is meant for
# waybar.
set -ux

check_recent_backup() {
    # note: the MESSAGE_ID here is specifically the message id for the
    # 'succeeded' message; we use this because it shouldn't change and
    # doesn't depend on language.
    [[ $(
	journalctl \
	    UNIT="borgbackup-job-$1.service" \
	    MESSAGE_ID=7ad2d189f7e94e70a38c781354912448 \
	    --since="-7d" \
	    --quiet \
	    --lines=1
       ) ]]
}

failed=
failure_tooltip="BAD: "

if  ! check_recent_backup key-of-chronology ; then
    failed=1
    failure_tooltip+=" key-of-chronology";
fi
if  ! check_recent_backup rsync-net ; then
    failed=1
    failure_tooltip+=" rsync.net";
fi

echo " "
if [[ $failed ]]; then
    echo $failure_tooltip
    echo "error"
else
    echo "Backups OK"
    echo
fi
