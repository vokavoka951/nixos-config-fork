#!/usr/bin/env bash

# Outputs the number of todos that are overdue, due today, and due within the
# next 7 weeks, space-separated. Requires `jq` to exist, and takes the path to
# the API token as the first argument.

set -euo pipefail
token=$(cat $1)
api_base="https://api.todoist.com/rest/v1"

function count_tasks {
  filter=$1
  curl "$api_base/tasks" \
    --silent \
    --get \
    --data-urlencode "filter=$1" \
    --header "Authorization: Bearer $token" \
    | jq length
}
overdue=$(count_tasks overdue)
today=$(count_tasks today)
week=$(count_tasks "7 days & !overdue & !today")
echo " "
echo "$overdue/$today/$week"
if (( $overdue > 0 )); then
    echo "overdue"
elif (( $today > 0 )); then
    echo "today";
else
    echo ""
fi

